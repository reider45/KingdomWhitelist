package me.reider45.KingdomWhitelist;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements CommandExecutor, Listener {

	static Boolean enabled = false;

	public void onEnable()
	{
		saveDefaultConfig();
		Bukkit.getPluginManager().registerEvents(this, this);
	}

	public void onDisable()
	{
		saveConfig();
		toEnd.clear();
	}

	static List<Long> toEnd = new ArrayList<Long>();
	@EventHandler
	public void onConnect(PlayerLoginEvent e)
	{
		if(enabled)
		{
			if(!e.getPlayer().hasPermission("whitelist.bypass"))
			{
				if(!toEnd.isEmpty())
				{
					long saved = toEnd.get(0);

					long difference = saved - Calendar.getInstance().getTimeInMillis(); // Then minus now

					double hours = Math.floor((difference/3600000) % 24);
					double mins = Math.floor((difference/60000) % 60);
					double secs = Math.floor((difference/1000) % 60);

					String time = getConfig().getString("Whitelist.Kick").replace("$", "�")
							.replace("%HOURS%", hours+"")
							.replace("%MINS%", mins+"")
							.replace("%SECS%", secs+"");

					e.disallow(Result.KICK_OTHER, time);
				}else{
					e.disallow(Result.KICK_OTHER, getConfig().getString("Whitelist.NoTime").replace("$", "�"));
				}
			}
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player p = (Player) sender;
			if(cmd.getName().equalsIgnoreCase("wl"))
			{
				if(args.length == 1)
				{
					if(args[0].equals("on"))
					{
						if(p.hasPermission("whitelist.on"))
						{
							enabled = true;
							p.sendMessage(getConfig().getString("Whitelist.OnMSG").replace('$', '�'));
						}
						
					}else
						if(args[0].equals("off"))
						{
							if(p.hasPermission("whitelist.off"))
							{
								enabled = false;
								p.sendMessage(getConfig().getString("Whitelist.OffMSG").replace('$', '�'));
							}
						}
				}else
					if(args.length == 3)
					{
						if(args[0].equals("set"))
						{
							if(args[1].equals("time"))
							{
								String time = args[2];

								Integer hours, mins, secs;

								if(!time.contains("h") || !time.contains("m") || !time.contains("s"))
								{
									p.sendMessage("Invalid format! Use 00h00m00s!");
									return false;
								}
								
								Integer hpos, mpos, spos;
								hpos = time.indexOf("h");
								mpos = time.indexOf("m");
								spos = time.indexOf("s");
								
								if(hpos > mpos || mpos > spos)
								{
									p.sendMessage("Invalid format! Use 00h00m00s!");
									return false;
								}
								

								hours = Integer.parseInt( time.split("h")[0] );
								time = time.replaceFirst(hours+"h", "");

								mins = Integer.parseInt( time.split("m")[0] );
								time = time.replaceFirst(mins+"m", "");

								secs = Integer.parseInt( time.split("s")[0] );
								
								p.sendMessage(getConfig().getString("Whitelist.Set").replace("$", "�")
										.replace("%HOURS%", hours+"")
										.replace("%MINS%", mins+"")
										.replace("%SECS%", secs+""));

								toEnd.clear();
								Calendar cal = Calendar.getInstance();
								cal.set(Calendar.HOUR, cal.get(Calendar.HOUR)+hours);
								cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+mins);
								cal.set(Calendar.SECOND, cal.get(Calendar.SECOND)+secs);

								toEnd.add(cal.getTimeInMillis());
							}
						}
					}
			}
		}
		return false;
	}


}
